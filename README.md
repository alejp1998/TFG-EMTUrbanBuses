# TFG - Statistical arrival time analysis and anomaly detection in the EMT urban buses

Ejemplo de documento y diapositivas realizadas en LaTeX para mi TFG en la ETSIT UPM.

## Documento

Se incluye el código fuente del documento (que debe compilarse con LaTeX), además de su correspondiente PDF.

## Diapositivas

Se incluye el código fuente de las diapositivas realizadas con Beamer a partir de la plantilla Metropolis(que debe compilarse con XeLaTeX), además de su correspondiente PDF.
