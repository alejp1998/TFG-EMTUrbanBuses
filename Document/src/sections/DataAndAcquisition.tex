

\section{Static data}

The available static data has been obtained from the open data portal of the CRTM \cite{staticdata}. It follows a ``GTFS feed'' format,
which is a static data format created by Google to make it easier to analyse and develop software on public transport. Such data comes in a .zip file with .txt files inside that provide information about routes, stops, frequencies, fares, etc.

From this ``GTFS feed'' format, we have transformed the data into .csv files and .json dictionaries, with the aim of making it more manageable when performing analysis and developing code in Python.

\subsection{Stops}

Starting from the ``stops.txt'' file, which contains the code, name, description, zone and location of each stop, a ``DataFrame'' is built which is then stored in the ``stops.csv'' file. Thus, from the code of a stop, we can quickly access its location and detailed description.

\subsection{Lines}

Similarly, from the ``routes.txt'' and ``shapes.txt'' files, a ``DataFrame'' is built where the identifiers of each line are associated to its description and path, which is defined as a set of points linked in an orderly manner, each one of them indicating its distance to the beginning of the line. Again, we store them in a file with the name ``lines\textunderscore shapes.csv''.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.325]{../images/emturbanmap.png}
    \caption{Map of lines (red and blue) and stops (green).}
    \label{fig:emturbanmap}
\end{figure}

\subsection{Lines collected dictionary}

In order to have a quick access to the most important data of each line, we have built a dictionary that stores the destinations, length and stops ordered with their distances to the beginning of the route for each line and direction collected. This dictionary is stored in the file ``lines\textunderscore collected\textunderscore dictionary.json''.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.35]{../images/lines_collected_dict.png}
    \caption{Lines collected dictionary entry for line 1.}
    \label{fig:linescollecteddict}
\end{figure}

So we know the destinations of each line direction, the id of the line (in the example the line id is equal to the line short name) and the length, ordered list of stops and distance of every stop to the beginning of the route for each direction of the line.




\section{Real-time data retrieving process}

In this Section we describe in detail an automatic process to collect data of the times, distances and location from each bus stop over time, which has been designed with the help of the EMT's API and a script in Python.

\subsection{MobilityLabs EMT's API}

Our source of information to construct the data set was the MobilityLabs EMT's API \cite{emtapi} that, after creating an account, gave us access to multitude of methods to obtain information in real time on the urban buses. Unfortunately, the number of requests that can be made to this API per day is limited to 20000 per account, rising to 150000 if an application is registered on their website. 

Due to this limitation, we chose to register the server for the visualizations as an application on their page with our main account, while we created four more accounts, finally reaching the amount of 230000 hits per day.

To get the information of the buses that are going to arrive to a stop, the method ``Time Arrival Bus'' must be called, indicating the code of the desired stop. The answer almost always arrives without problems in about 122 milliseconds on average, it is in ``json'' format and looks like this:

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.3]{../images/timearrivalbus.png}
    \caption{Response to Time Arrival Bus method.}
    \label{fig:timearrivalbus}
\end{figure}

From these answers, we build a row for each bus identifier with the following fields:

\begin{itemize}
    \item \textbf{datetime:} Date and time when the request was answered.
    \item \textbf{bus:} Unique identifier of the bus.
    \item \textbf{line:} Line that the bus belongs to.
    \item \textbf{stop:} The stop that gives the information about the arrival of the bus.
    \item \textbf{isHead:} It indicates ``true'' if the bus is the first of the line that will reach the stop, and ``false'' if not.
    \item \textbf{destination:} The destination that the bus is headed to.
    \item \textbf{deviation:} Indicates a possible deviation of the bus from its path, it is always set to 0.
    \item \textbf{request\textunderscore time:} Time taken to answer the request in milliseconds.
    \item \textbf{estimateArrive:} ETA of the bus to the stop in seconds.
    \item \textbf{DistanceBus:} Distance of the bus to the stop along the line in meters.
    \item \textbf{lat:} Latitude where the bus is located at.
    \item \textbf{lon:} Longitude where the bus is located at.
\end{itemize}


\subsection{Lines selection}

Due to limitations in the EMT API, it is not possible to collect information about all city bus stops and lines. If we wanted to make requests to all the stops (4742 in total), knowing that the lines start at 7:00 and end at 23:00, we would be limited to make a request to each one of them every 20 minutes.

\begin{equation}
Minutes\; between\; requests = \frac{4742*(23-7)*60}{230000} \approx 20(mins)
\end{equation}

For this reason, we decided to analyze a smaller set formed by lines 1, 82, 91(F), 92(G), 99(U) and 132 from Monday to Friday, since they have many stops in common (we have to make fewer requests) and the author uses or is familiar with them. In total they add up to 207 different stops, so in this case we were able to make requests to all of them every 52 seconds.

\begin{figure}[H]
\centering
\begin{subfigure}{.4\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{../images/selectedlines.png}
  \caption{From 7:00 to 23:00.}
  \label{fig:selectedlines1}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{../images/selectedlinesnight.png}
  \caption{From 0:00 to 5:30 (weekends).}
  \label{fig:selectedlines2}
\end{subfigure}
\caption{Selected lines.}
\label{fig:selectedlines}
\end{figure}

On the other hand, due to the fact that on Saturdays and Sundays lines 91, 92 and 99 are inactive as they are specially dedicated to Ciudad Universitaria, requests are only made to the stops of lines 1, 82 and 132 (185 different stops) during the day, while at night the saved hits are used to study the night lines 502(N2) and 506(N6) (131 different stops) from 00:00 to 5:30.

\subsection{Script for the data collection}

Once the lines from which to collect information have been selected, we create a script in Python that, once started, automatically makes requests to all the stops at the desired time intervals and days, and then stores the data in a file called ``buses\textunderscore data.csv''.

This is done by using timers, which repeatedly execute the function passed to them as a parameter after a certain number of seconds. This function, receives the lines from which it has to collect the information and makes the requests concurrently, and, as it receives the answers from these, it stores them in the file mentioned before.

The requests are made every 55 seconds during the day, while on Saturday and Sunday nights they are made every 70 seconds. To the original information of the request, we add the attributes ``pos\textunderscore in\textunderscore burst'' which contains the position of the request inside each burst, ``request\textunderscore time'' with the time it took to make the request in milliseconds and ``given\textunderscore coords'' which is set to 1 if coordinates other than ``[0,0]'' have been received.

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{../images/datacollected.png}
  \caption{Example content of a row of the collected data.}
  \label{fig:datacollected}
\end{subfigure}
\begin{subfigure}{.4\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{../images/newselectedlines.png}
  \caption{Lines 44 and 133.}
  \label{fig:newselectedlines}
\end{subfigure}
\caption{Data format and new lines.}
\label{fig:dataformatnewlines}
\end{figure}

The script stores about 55MB of data per day, and has been running practically without a break since the evening of February 26th.

\subsection{Updates in the data retrieving process}

Due to the impact of COVID-19, all face-to-face activity at the university was cancelled as of March 11, so the university bus lines were also suspended. In addition, on March 24 the EMT API increased the number of daily visits for users who had registered an application, from 150,000 to 250,000. 

Therefore, lines 44 and 133 were added to those already selected, eliminating university lines 91(F), 92(G) and 99(U). So, since we now have 272 different stops and 330,000 hits per day, we perform a burst of requests to the API every 50 seconds.