

When analyzing the operation of bus lines, one of the most important aspects is the time it takes for buses to travel between consecutive stops (called {\em running time}), as this time depends on the level of traffic on the line, which will depend on the time and type of day (working days, holidays, etc.), as well as weather events (fog or heavy rain) or social events (a football match or a concert). In this section we are going to explain a procedure to estimate these running times, as well as performing a slight analysis on them.


\section{Development of the estimator}
\label{sub:timebtstopsestim}

To build the estimates of the times between stops we make use of the estimator built in \ref{sub:arrival_time_estimation}. If we use the same notation as in \cite{yu2011bus}, the arrival time at a stop ($s$) of a bus ($n$) belonging to the line ($l$) is: $T^{a}_{s,l,n}$. Thus, we can estimate the real running time between two stops A and B as:

\begin{equation}
    t^{r}_{l,n} = T^{a}_{B,l,n} - T^{a}_{A,l,n}
\end{equation}

In turn, we have obtained the running time between stops estimated in real time by the API as the subtraction of the remaining times to arrive at each stop given by stops A and B at the time the bus is considered to have arrived at stop A, denoted as $ETA^{t=t_i}_{s,l,n}$. Thus, the value of the estimation of the running time between stops made by the API is:

\begin{equation}
    \widehat{t^{r}_{l,n}} = ETA^{t = T^{a}_{A,l,n}}_{B,l,n} - ETA^{t = T^{a}_{A,l,n}}_{A,l,n}
\end{equation}

Following this method, using the script ``times\textunderscore bt\textunderscore stops.py'' we calculate all the running times between stops on the collected clean data, storing the results in the file ``times\textunderscore bt\textunderscore stops.csv'' with the following attributes for each row:

\begin{itemize}
    \item \textbf{line: } Id of the line the bus belongs to
    \item \textbf{direction: } Direction of the line
    \item \textbf{date: } Date when the time was obtained
    \item \textbf{st\textunderscore hour} and \textbf{end\textunderscore hour: } Hour interval when the time between stops was obtained.
    \item \textbf{stopA: } Origin stop
    \item \textbf{stopB: } Destination stop
    \item \textbf{bus: } Id of the bus
    \item \textbf{trip\textunderscore time: } Real running time estimation
    \item \textbf{api\textunderscore trip\textunderscore time: } Estimation of the running time given by the API.
\end{itemize}


\section{Analysis of the running times}

As we said, the running time between stops depends on many factors. The main ones are the type of day and the hours range, so we are going to visualize their impact on both the real and API estimations running times, focusing in the trayect between stops 193 and 173 of line 1.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.25]{../images/tbs-LA.png}
    \caption{Running time from stop 193 to 173 of line 1 in working days (LA).}
    \label{fig:tbsLA}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.25]{../images/tbs-FE.png}
    \caption{Running time from stop 193 to 173 of line 1 in festive days (FE).}
    \label{fig:tbsFE}
\end{figure}

From these figures we can conclude that, as expected, the running times of the festive days are lower than the ones of the working days. The running times also vary depending on the hour of day, being higher in the ranges of hours with the highest level of social activity, as the line goes through the main street of the center of Madrid, ``Gran Vía''.

In addition, we can see that the estimations made by the API also depend on the day an the hours range, having their medians close to the ones of the real running time estimations.


\section{Comparison with the estimations made by the API}

To test the quality of the running time estimations made by the API, we are going to compare them with the estimated real running time between stops, so the error between estimations is defined as follows: 

\begin{equation}
    \epsilon_{s,l,n} = \widehat{t^{r}_{l,n}} - t^{r}_{l,n}
\end{equation}

So, as in \ref{ssub:arrival_time_estimation_quality}, when the error is positive the estimated time between the stops is higher than the real one and when the error is negative the estimated time between stops is lower than the real one.

If we compute this error for all the data collected before the COVID-19 event, we obtain the following: 

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.25]{../images/tbsapierror.png}
    \caption{Running time estimations error between our real running time estimation and the one provided by the API.}
    \label{fig:tbsapierror}
\end{figure}

Where most of the errors tend to be lower than 5 seconds (peak in the middle), and are distributed with a lower frequency the higher the error is. 

We have also obtained the root mean squared error, the mean error and the standard deviation of the error:

\begin{itemize}
    \item \textbf{Mean error: } $\bar{\epsilon} = -8.16(s)$
    \item \textbf{Standard deviation of the error: } $s_\epsilon = 65.53(s)$
    \item \textbf{Root Mean Squared Error: } $RMSE = 66.05(s)$
\end{itemize}

As the mean error obtained is negative, we can conclude that the estimated running times between stops given by the API are usually slightly lower than the real running time between the stops, which is coherent with the conclusions obtained from the analysis in \ref{ssub:arrival_time_estimation_quality}.



